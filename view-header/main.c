#include <stdbool.h>
#include <stdio.h>

#include "bmp.h"
#include "util.h"
#include "image_rot.h"
#include "image.h"

void usage() {
    fprintf(stderr, "Usage: ./rotate BMP_FILE_NAME\n");
}

int main(int argc, char **argv) {
    if (argc != 2) usage();
    if (argc < 2) err("\n Not enough arguments \n");
    if (argc > 2) err("\n Too many arguments \n");

    FILE *f = fopen(argv[1], "rb");
    if (!f) err("\n Failed to open BMP file \n");

    struct image img = {0};
    enum read_status readStatus = from_bmp(f, &img);
    if (readStatus != READ_OK) {
        fclose(f);
        err("\n Failed to read image or header.\n");
    }
    struct image rotated_img = rotate(&img);
    fclose(f);
    f = fopen("rotated_img", "wb");
    enum write_status writeStatus = to_bmp(f, &rotated_img);
    if (writeStatus != WRITE_OK) {
        fclose(f);
        err("\n Failed to write image.\n");
    } else {
        printf("\n Image was successfully rotated 90 degrees \n");
    }
    fclose(f);
    return 0;

}
