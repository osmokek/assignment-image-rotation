#ifndef VIEW_HEADER_IMAGE_ROT_H
#define VIEW_HEADER_IMAGE_ROT_H

struct image rotate(struct image const *source);

#endif
