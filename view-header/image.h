#ifndef VIEW_HEADER_IMAGE_H
#define VIEW_HEADER_IMAGE_H
struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};

#endif
