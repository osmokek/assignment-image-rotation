#include <stdint.h>
#include <stdlib.h>
#include "image_rot.h"
#include "bmp.h"
#include "util.h"

struct image rotate(struct image const *source) {
    struct pixel pxl = {0};
    uint64_t width = source->width;
    uint64_t height = source->height;

    struct image rot_img = {
            .width = source->width,
            .height = source->height,
            .data = malloc(width * height * sizeof(struct pixel))
    };
    if (rot_img.data == NULL) {
        err("Failed to rotate image");
    }
    rot_img.width = height;
    rot_img.height = width;
    for (uint64_t x = 0; x < height; x++) {
        for (uint64_t y = 0; y < width; y++) {
            pxl = source->data[width * x + y];
            rot_img.data[height * y + height - x - 1] = pxl;
        }
    }
    return rot_img;
}
