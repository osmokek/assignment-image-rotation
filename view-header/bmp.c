#include "bmp.h"
#include "util.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define FOR_BMP_HEADER(FOR_FIELD) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD(t, n) t n ;
#define BMPTYPE 0x4D42

struct __attribute__((packed)) bmp_header {
    FOR_BMP_HEADER(DECLARE_FIELD)
};

static bool read_header(FILE *f, struct bmp_header *header) {
    return fread(header, sizeof(struct bmp_header), 1, f);
}

bool read_header_from_file(FILE *in, struct bmp_header *header) {
    if (!in) return false;
    if (read_header(in, header)) {
        fclose(in);
        return true;
    }
    fclose(in);
    return false;
}

uint32_t getPadding(const uint64_t width) {
    size_t widthSize = width * sizeof(struct pixel);
    if (widthSize % 4 == 0) return 0;
    return 4 - (widthSize % 4);
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {0};
    const uint32_t pSize = sizeof(struct pixel);
    enum read_status rs = READ_OK;
    const uint32_t hSize = sizeof(struct bmp_header);
    if (fread(&header, hSize, 1, in) < 1) {
        if (feof(in)) rs = READ_INVALID_BITS;
        rs = READ_INVALID_HEADER;
    }
    if (fseek(in, header.bOffBits, SEEK_SET) != 0) rs = READ_INVALID_BITS;
    if (header.biBitCount != 24) rs = READ_INVALID_HEADER;
    if (header.bfType != BMPTYPE) rs = READ_INVALID_HEADER;

    img->height = header.biHeight;
    img->width = header.biWidth;
    img->data = malloc(img->width * img->height * pSize);
    const uint32_t padding = getPadding(img->width);
    for (size_t j = 0; j < img->height; j++) {
        if (fread(img->data + j * img->width, pSize, img->width, in) < pSize) {
            free(img->data);
            rs = READ_INVALID_BITS;
        }

        if (fseek(in, padding, SEEK_CUR) != 0) rs = READ_INVALID_BITS;
    }
    if (fseek(in, 0, SEEK_SET) != 0) rs = READ_INVALID_BITS;
    return rs;

}

struct bmp_header new_header(struct image const *img) {
    struct bmp_header const header =
            {
                    .bfType = BMPTYPE,
                    .biBitCount = 24,
                    .biHeight = img->height,
                    .biWidth = img->width,
                    .bOffBits = sizeof(struct bmp_header),
                    .bfileSize = sizeof(struct bmp_header) +
                                 (sizeof(struct pixel) * img->width + img->width % 4) * img->height,
                    .biSizeImage = img->width * img->height * sizeof(struct pixel),
                    .biSize = 40,
                    .biPlanes = 1
            };
    return header;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header header = new_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    const uint32_t hSize = sizeof(struct bmp_header);
    const uint32_t pSize = sizeof(struct pixel);
    if (fwrite(&header, hSize, 1, out) < 1) return WRITE_ERROR;
    const char paddingBytes[3] = {0};
    const uint32_t padding = getPadding(img->width);

    for (size_t j = 0; j < img->height; j++) {
        if (fwrite(img->data + j * img->width, pSize, img->width, out) != img->width) return WRITE_ERROR;
        if (fwrite(paddingBytes, padding, 1, out) != 1 && padding != 0) return WRITE_ERROR;
        if (fflush(out) != 0) return WRITE_ERROR;
    }
    if (fseek(out, 0, SEEK_SET) != 0) return WRITE_ERROR;
    return WRITE_OK;
}
